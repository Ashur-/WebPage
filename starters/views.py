from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import StarterForm
from starters.models import Starter


# Tasks views v
@login_required
def create_starter(request):
    if request.method == "POST":
        form = StarterForm(request.POST)
        if form.is_valid():
            starter = form.save(commit=False)
            starter.owner = request.user
            starter.save()
            return redirect("list_pokeballs")
    else:
        form = StarterForm()
    return render(request, "starters/create.html", {"form": form})


@login_required
def show_my_starters(request):
    starters = Starter.objects.filter(assignee=request.user)
    context = {"starters": starters}
    return render(request, "starters/my_starters.html", context)
