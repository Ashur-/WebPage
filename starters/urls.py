from django.urls import path
from .views import create_starter, show_my_starters


# Tasks urls v
urlpatterns = [
    path("create/", create_starter, name="create_starter"),
    path("mine/", show_my_starters, name="show_my_starters"),
]
