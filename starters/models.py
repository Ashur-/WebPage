from django.db import models
from django.contrib.auth.models import User
from pokeballs.models import Pokeball


# Starters Models v
class Starter(models.Model):
    name = models.CharField(max_length=200)
    picture = models.URLField(default=None)
    pokeball = models.ForeignKey(
        Pokeball, on_delete=models.CASCADE, related_name="starters"
    )
    assignee = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="starters"
    )

    def __str__(self):
        return self.name
