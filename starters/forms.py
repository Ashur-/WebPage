from django import forms
from .models import Starter


# Tasks forms v
class StarterForm(forms.ModelForm):
    class Meta:
        model = Starter
        exclude = ["is_completed", "owner"]
