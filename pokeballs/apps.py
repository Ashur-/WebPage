from django.apps import AppConfig


class PokeballsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "pokeballs"
