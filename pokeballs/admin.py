from django.contrib import admin
from pokeballs.models import Pokeball

# register models
admin.site.register(Pokeball)
