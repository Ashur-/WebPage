from django.db import models
from django.contrib.auth.models import User

# create models v


class Pokeball(models.Model):
    name = models.CharField(max_length=200)
    picture = models.URLField(default=None)
    description = models.TextField()
    owner = models.ForeignKey(
        User,
        related_name="pokeballs",
        on_delete=models.CASCADE,
        null=True,
    )
    members = models.ManyToManyField(User, through="Membership")

    def __str__(self):
        return self.name


class Membership(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    pokeball = models.ForeignKey(Pokeball, on_delete=models.CASCADE)
