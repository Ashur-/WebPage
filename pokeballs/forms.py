from django import forms
from pokeballs.models import Pokeball


class PokeballForm(forms.ModelForm):
    class Meta:
        model = Pokeball
        fields = ["name", "picture", "description", "owner"]
