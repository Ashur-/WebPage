from django.shortcuts import render, redirect, get_object_or_404
from pokeballs.models import Pokeball
from django.contrib.auth.decorators import login_required
from django.db import models
from pokeballs.forms import PokeballForm


@login_required
def pokeball_list(request):
    pokeballs = Pokeball.objects.filter(
        models.Q(owner=request.user)
        | models.Q(
            members=request.user
        ) 
    ).distinct()
    context = {"pokeballs": pokeballs}
    return render(request, "pokeball/list.html", context)


@login_required
def show_pokeball(request, id):
    pokeball = get_object_or_404(Pokeball, id=id)
    starters = pokeball.starters.all()
    context = {
        "pokeball": pokeball,
        "starters": starters,
        "pokeball_name": pokeball.name,
    }
    return render(request, "pokeball/show_pokeball.html", context)


@login_required
def create_pokeball(request):
    if request.method == "POST":
        form = PokeballForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_pokeballs")
    else:
        form = PokeballForm()
    return render(request, "pokeball/create.html", {"form": form})
