from django.urls import path
from pokeballs.views import pokeball_list, show_pokeball, create_pokeball

# patterns v
urlpatterns = [
    path("", pokeball_list, name="list_pokeballs"),
    path("<int:id>/", show_pokeball, name="show_pokeball"),
    path("create/", create_pokeball, name="create_pokeball"),
]
