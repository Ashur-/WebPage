const bubbles = document.querySelectorAll('.bubble');

bubbles.forEach(feather => {
    bubble.addEventListener('mousedown', holdBubble);
    bubble.addEventListener('mouseup', releaseBubble);
});

function holdBubble() {
    this.style.cursor = 'grabbing';
}

function releaseBubble() {
    this.style.cursor = 'grab';
}